/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8265406576097657, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [1.0, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.027450980392156862, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [1.0, 500, 1500, "suggestProgram"], "isController": false}, {"data": [1.0, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [1.0, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.9980237154150198, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [1.0, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [1.0, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [1.0, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [1.0, 500, 1500, "leadByID"], "isController": false}, {"data": [1.0, 500, 1500, "registrationByID"], "isController": false}, {"data": [1.0, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.025490196078431372, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.5, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [1.0, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.998015873015873, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 5079, 0, 0.0, 539.2012207127392, 2, 5547, 33.0, 2149.0, 3232.0, 5339.2, 16.777381750674696, 104.00352398514843, 34.070153297173384], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 509, 0, 0.0, 4.726915520628686, 2, 20, 4.0, 6.0, 13.0, 16.0, 1.7322234398077878, 1.0200495451211877, 2.1376715898765326], "isController": false}, {"data": ["getCentreHolidaysOfYear", 253, 0, 0.0, 40.640316205533594, 33, 165, 37.0, 46.0, 55.0, 133.4800000000003, 0.861352834633874, 2.1986886232909124, 1.1061318140073675], "isController": false}, {"data": ["getPastChildrenData", 255, 0, 0.0, 2110.6431372549027, 1207, 2489, 2172.0, 2275.0, 2297.2, 2421.7999999999997, 0.8698173383589446, 1.7437685742244127, 1.9834213721368512], "isController": false}, {"data": ["suggestProgram", 254, 0, 0.0, 11.787401574803154, 8, 129, 10.0, 16.0, 20.0, 34.14999999999992, 0.8889448854171041, 0.7205314989220668, 1.1033681146143939], "isController": false}, {"data": ["findAllWithdrawalDraft", 255, 0, 0.0, 5.11764705882353, 3, 18, 5.0, 6.0, 7.0, 15.879999999999995, 0.8743176893326385, 0.5660865508081439, 2.8543398783584086], "isController": false}, {"data": ["findAllLevels", 254, 0, 0.0, 9.881889763779528, 7, 57, 8.0, 15.0, 20.0, 33.44999999999999, 0.852615077859869, 0.5428760066060885, 0.7143981804724293], "isController": false}, {"data": ["findAllClass", 254, 0, 0.0, 49.54330708661418, 32, 230, 40.0, 53.5, 124.0, 227.45, 0.8653996851853114, 86.19600594834519, 0.6997567766928104], "isController": false}, {"data": ["getStaffCheckInOutRecords", 253, 0, 0.0, 7.837944664031617, 3, 521, 4.0, 7.599999999999994, 14.299999999999983, 67.28000000000014, 0.8523770538749464, 0.5052664762715747, 1.1154152853441683], "isController": false}, {"data": ["getChildrenToAssignToClass", 254, 0, 0.0, 8.303149606299217, 6, 24, 7.0, 11.0, 16.0, 19.44999999999999, 0.8526064918935249, 0.4854195163807861, 0.6253002689570675], "isController": false}, {"data": ["getCountStaffCheckInOut", 252, 0, 0.0, 5.670634920634918, 3, 39, 5.0, 8.0, 13.349999999999994, 30.349999999999994, 0.8585240880736695, 0.507233470004463, 0.8752921366688584], "isController": false}, {"data": ["searchBroadcastingScope", 256, 0, 0.0, 5087.222656250003, 3166, 5547, 5241.5, 5388.3, 5430.15, 5528.6, 0.8471659651073518, 1.6369211049807402, 1.6604121991898975], "isController": false}, {"data": ["getTransferDrafts", 252, 0, 0.0, 5.7103174603174605, 3, 42, 5.0, 9.0, 15.0, 22.349999999999994, 0.8677417022199725, 0.5575918359968183, 1.5058369187938392], "isController": false}, {"data": ["leadByID", 254, 0, 0.0, 221.09448818897638, 141, 404, 217.0, 264.0, 281.0, 358.7999999999997, 0.8721688848599722, 2.3656461013793315, 3.938387620695812], "isController": false}, {"data": ["registrationByID", 255, 0, 0.0, 51.21176470588236, 35, 256, 46.0, 58.0, 71.19999999999999, 182.95999999999992, 0.8815324144654284, 1.4776335996944021, 4.009939440019497], "isController": false}, {"data": ["getRegEnrolmentForm", 252, 0, 0.0, 37.66666666666664, 26, 87, 35.0, 45.0, 47.349999999999994, 71.94, 0.8582521626592194, 1.5390454711872488, 3.6693632501192015], "isController": false}, {"data": ["findAllLeads", 255, 0, 0.0, 2068.8156862745095, 1215, 2391, 2123.0, 2226.6, 2239.2, 2327.16, 0.8639586926102734, 1.7263060652017765, 1.9557189936236459], "isController": false}, {"data": ["getEnrollmentPlansByYear", 255, 0, 0.0, 896.5960784313728, 565, 1231, 901.0, 964.4, 985.5999999999999, 1116.44, 0.8721317979253523, 0.9751897741691662, 1.4470233639405992], "isController": false}, {"data": ["getAvailableVacancy", 255, 0, 0.0, 10.772549019607835, 7, 65, 9.0, 16.0, 19.19999999999999, 29.0, 0.8889570616308701, 0.487016515209881, 1.176305486826005], "isController": false}, {"data": ["getRegistrations", 252, 0, 0.0, 84.85317460317458, 61, 589, 77.0, 92.70000000000002, 110.69999999999999, 301.3699999999999, 0.8724613797353533, 2.0554646170604283, 2.6940653151593623], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 5079, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
